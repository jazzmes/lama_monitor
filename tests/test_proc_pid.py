from sys import argv
from lama_monitor.proc_parser import get_process_disk_metrics
from lama_monitor.proc_parser import get_process_net_metrics

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


print(get_process_disk_metrics(argv[1]))
print(get_process_net_metrics(argv[1]))

