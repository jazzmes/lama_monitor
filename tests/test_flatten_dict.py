from lama_monitor.output import CSVOutput

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


d = {
    'a': {
        'a1': 1
    },
    'b': 2,
    'c': {
        'c1': {
            'c2': 3
        }
    }
}

flat_d = {
    'a:a1': 1,
    'b': 2,
    'c:c1:c2': 3
}


class CSVOutputTest(CSVOutput):
    pass

out = CSVOutputTest._flatten_dict(d)

assert set(out.keys()) == set(flat_d.keys()), "FAIL: %s is not the flat version of %s" % (out, d)

print("SUCCESS: %s" % flat_d)