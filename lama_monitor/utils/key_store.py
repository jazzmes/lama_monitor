from redis import StrictRedis
import logging

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'

logger = logging.getLogger(__name__)


class KeyStore(object):

    def __init__(self, host='localhost', port=6379):
        self.redis = None
        try:
            self.redis = StrictRedis(decode_responses=True, host=str(host), port=port)
        except:
            logger.error("Error connecting to redis!")

    def put_dict(self, key, d):
        key = key.lower()
        try:
            self.redis.hmset(key, d)
        except ConnectionError as e:
            logger.error("Connection Error: %s", e)

    def get_dict(self, key):
        key = key.lower()
        try:
            return {str(k): str(v) if v != 'None' else None for k, v in self.redis.hgetall(key).items()}
        except ConnectionError as e:
            logger.error("Connection Error: %s", e)

    def put(self, key, value):
        key = key.lower()
        try:
            self.redis.set(key, value)
        except ConnectionError as e:
            logger.error("Connection Error: %s", e)

    def get(self, key):
        key = key.lower()
        try:
            val = self.redis.get(key)
            return str(val) if val is not None else None
        except ConnectionError as e:
            logger.error("Connection Error: %s", e)

    def delete(self, key):
        key = key.lower()
        try:
            val = self.redis.delete(key)
            return str(val) if val is not None else None
        except ConnectionError as e:
            logger.error("Connection Error: %s", e)

    def keys(self, pattern):
        pattern = pattern.lower()
        logger.debug("Get key with pattern '%s'", pattern)
        try:
            return set(self.redis.keys(pattern=pattern))
        except ConnectionError as e:
            logger.error("Connection Error: %s", e)