import signal
import logging
from argparse import ArgumentParser, ArgumentTypeError
from datetime import timedelta, datetime
from logging import Logger
from threading import Timer

# from lama_monitor.output_plugins import RedisOutput, CSVOutput
# from lama_monitor.proc_plugins import AllAppAgentProcMonitorPlugin, ProviderAgentProcMonitorPlugin
# from lama_monitor import output_plugins
from lama_monitor import proc_plugins

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


logger = logging.getLogger(__name__)


ZeroDelta = timedelta(seconds=0)


class Engine(object):
    """
    Manages the execution of all the plugins.
    """

    def __init__(self, plugins=(), output_plugins=(), interval=timedelta(seconds=10)):
        self.plugins = plugins or self.default_plugins
        self.output_plugins = output_plugins or self.default_output_plugins
        self.interval = interval
        self._timer = None

        self._initialize()
        self.__next_run = None

    @property
    def default_plugins(self):
        return ()

    @property
    def default_output_plugins(self):
        return ()

    def _initialize(self):
        for plugin in self.plugins:
            plugin.initialize()

    def start(self):
        self.__next_run = datetime.now()
        self._run()

    def _run(self):
        logger.debug("Running at: %s" % datetime.now())
        while self.__next_run < datetime.now():
            self.__next_run = self.__next_run + self.interval

        results = {}
        for plugin in self.plugins:
            results.update(plugin.run())

        # output
        for oplugin in self.output_plugins:
            oplugin.process_results(results)

        delay = max(self.__next_run - datetime.now(), ZeroDelta)
        self._timer = Timer(delay.total_seconds(), self._run)
        self._timer.start()

    def stop(self):
        logger.debug("Stopping monitor engine")
        if self._timer:
            self._timer.cancel()

        for plugin in self.plugins:
            plugin.stop()


def mon_plugins_list(arg_str):
    chosen_plugins = []
    try:
        plugins = arg_str.split(';')
        for plugin in plugins:
            cls = plugin.split(':')
            cls, cls_args = cls[0], cls[1].split(',') if len(cls) > 1 else ()
            try:
                chosen_plugins.append(getattr(proc_plugins, cls)(*cls_args))
            except TypeError as e:
                raise ArgumentTypeError("%s::%s" % (cls, str(e)))
    except ArgumentTypeError as e:
        raise e
    except Exception as e:
        raise ArgumentTypeError(e)

    return set(chosen_plugins)


def log_level(arg):
    try:
        Logger('test', level=arg)
    except Exception as e:
        raise ArgumentTypeError(e)

    return arg


def main():

    ap = ArgumentParser(description="Monitor for LAMA projects.")
    group = ap.add_argument_group('Output Plugins (specify at least 1)')
    group.add_argument('-o', dest='out', action='store_true',
                        help='Print to stdout.')
    group.add_argument('-c', dest='csv', type=str,
                    help='Save to CSV - Name of CSV file.', default=None)
    group.add_argument('-j', dest='json', type=str,
                       help='Save to list of events in JSON format (1 event per line JSON format) - Name of file.', default=None)
    group.add_argument('-r', dest='redis', action='store_true',
                       help='Publish results to REDIS')
    ap.add_argument('-m', dest='mon_plugins', type=mon_plugins_list, required=True,
                    help='Monitoring plugins to load: plugin1;plugin2:arg1,arg2;... ')
    ap.add_argument('-i', dest='interval', type=int,
                    help='Interval to monitor in seconds', default=10)
    ap.add_argument('--ll', dest='log_level', type=log_level,
                    help='Log level', default='INFO')

    ap.add_argument('--lf', dest='log_file', type=str,
                    help='Log file', default=None)

    args = ap.parse_args()

    output_plugins = []
    if args.csv:
        from lama_monitor.output_plugins import CSVOutput
        output_plugins.append(CSVOutput(args.csv))
    if args.json:
        from lama_monitor.output_plugins import JSONEventsOutput
        output_plugins.append(JSONEventsOutput(args.json))
    if args.redis:
        from lama_monitor.output_plugins import RedisOutput
        output_plugins.append(RedisOutput())
    if args.out or not output_plugins:
        from lama_monitor.output_plugins import StreamOutput
        output_plugins.append(StreamOutput())

    if not output_plugins:
        logger.error("Please specify at list one output plugin!\n\n%s", ap.format_help())
        exit(1)

    # setup log
    rl = logging.getLogger(__name__.split('.')[0])

    if args.log_file:
        h = logging.FileHandler(args.log_file)
    else:
        h = logging.StreamHandler()

    formatter = logging.Formatter('%(asctime)s : %(message)s')
    h.setFormatter(formatter)
    rl.setLevel(args.log_level)
    rl.addHandler(h)

    logger.debug("Log level: %s", logger.getEffectiveLevel())
    logger.debug("Proc:   %s", args.mon_plugins)
    logger.debug("Output: %s", output_plugins)
    e = Engine(
        # plugins=(
        #     AllAppAgentProcMonitorPlugin(),
        #     ProviderAgentProcMonitorPlugin()
        # ),
        # output_plugins=(
        #     RedisOutput(),
        #     CSVOutput("test_lama_monitor.csv")
        # )
        plugins=args.mon_plugins,
        output_plugins=output_plugins,
        interval=timedelta(seconds=args.interval)
    )

    def clean_stop(signum, frame):
        e.stop()

    signal.signal(signal.SIGINT, clean_stop)
    e.start()


if __name__ == "__main__":
    # logger.setLevel(logging.DEBUG)
    # stream_handler = logging.StreamHandler()
    # stream_handler.setFormatter(logging.Formatter("%(asctime)s : %(levelname)5.5s : %(module)14.14s[%(lineno)3.3s] : %(message)s"))
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s : %(levelname)5.5s : %(module)14.14s[%(lineno)3.3s] : %(message)s"
    )
    # logger.addHandler(stream_handler)

    main()


