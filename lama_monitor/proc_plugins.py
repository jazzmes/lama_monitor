import logging
import psutil as pu
import traceback
from os.path import basename, splitext
from netaddr import IPSet

from lama_monitor.base_plugin import Plugin
from lama_monitor.network import NetworkPortStats
from lama_monitor.proc_parser import get_process_disk_metrics, get_process_net_metrics

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'

logger = logging.getLogger(__name__)


class BadPluginException(Exception):
    pass


class ProcMonitorPlugin(Plugin):
    def __init__(self, pid):
        super().__init__()
        self.proc = pu.Process(pid)
        self.proc_name = self.proc.name()
        self.present_proc_name = None

        self._network_plugin = NetworkPortStatsByProcNamePlugin(self.proc_name)

    def run(self):
        results = {}
        try:
            results['cpu'] = vars(self.proc.cpu_times())
        except:
            logger.error("Error on results: %s", traceback.format_exc())
        try:
            results['mem'] = vars(self.proc.memory_info())
        except:
            logger.error("Error on results: %s", traceback.format_exc())
        try:
            results['disk'] = get_process_disk_metrics(self.proc.pid)
        except:
            logger.error("Error on results: %s", traceback.format_exc())
        try:
            #     results['network'] = get_process_net_metrics(self.proc.pid)
            results['network'] = self._network_plugin.run().get(self.proc_name)
        except:
            logger.error("Error on results: %s", traceback.format_exc())

        return {self.present_proc_name or self.proc_name: results}


class ProviderAgentProcPlugin(ProcMonitorPlugin):
    def __init__(self, pid):
        super().__init__(pid)
        self.present_proc_name = 'provider'


class AppAgentProcPlugin(ProcMonitorPlugin):
    def __init__(self, pid):
        super().__init__(pid)
        self.present_proc_name = self.extract_name()

    def extract_name(self):
        for w in self.proc.cmdline():
            if w.endswith('.json'):
                return splitext(basename(w))[0]
        return None


class AllAppAgentProcMonitorPlugin(Plugin):
    def __init__(self):
        super().__init__()
        self.monitors = {}

    def run(self):
        results = {}
        logger.debug("Find app agents!")
        for proc in pu.process_iter():
            logger.debug(proc.name())
            if 'run_application' in proc.name():
                if proc.status() == pu.STATUS_ZOMBIE:
                    logger.warn("PID %d detected with %s but is ZOMBIE!" % (proc.pid, proc.name()))
                    continue
                if proc.is_running() and proc.pid not in self.monitors:
                    self.monitors[proc.pid] = AppAgentProcPlugin(proc.pid)
                mon = self.monitors.get(proc.pid)
                results.update(
                    mon.run()
                )
        return {'app_agent': results}


class ProcByNameMonitorPlugin(ProcMonitorPlugin):
    def __init__(self, proc_name):
        proc_pid = None
        for proc in pu.process_iter():
            try:
                if proc_name in proc.name():
                    if proc.status() == pu.STATUS_ZOMBIE:
                        logger.warn("PID %d detected with %s but is ZOMBIE!" % (proc.pid, proc.name()))
                        continue
                    if proc.is_running():
                        proc_pid = proc.pid
                        break
            except pu.ZombieProcess:
                pass
        if proc_pid:
            super().__init__(proc_pid)
        else:
            raise BadPluginException("ProcByNameMonitorPlugin:Process not found: {}".format(proc_name))


class ProviderAgentProcMonitorPlugin(Plugin):
    def __init__(self):
        super().__init__()
        self.monitor = None

    def run(self):
        results = {}

        pids = []
        for proc in pu.process_iter():
            if 'run_provider' in proc.name():
                if proc.status() == pu.STATUS_ZOMBIE:
                    logger.info("PID %d detected with %s but is ZOMBIE!" % (proc.pid, proc.name()))
                    continue
                if proc.is_running():
                    pids.append(proc.pid)

        if len(pids) > 1:
            logger.info("Multiple providers running: %s" % pids)
        elif not len(pids):
            logger.info("Provider not running")
        else:
            if not self.monitor:
                self.monitor = ProviderAgentProcPlugin(pids[0])
            results = self.monitor.run()

        return results


class AllProcMonitorPlugin(Plugin):
    def __init__(self, name):
        super().__init__()
        self.name = name
        self.monitors = {}

    def run(self):
        results = {}
        for proc in pu.process_iter():
            try:
                if self.name in proc.name():
                    if proc.status() == pu.STATUS_ZOMBIE:
                        logger.warn("PID %d detected with %s but is ZOMBIE!" % (proc.pid, proc.name()))
                        continue
                    if proc.is_running() and proc.pid not in self.monitors:
                        self.monitors[proc.pid] = ProcMonitorPlugin(proc.pid)
                    mon = self.monitors.get(proc.pid)
                    results.update(
                        mon.run()
                    )
            except pu.ZombieProcess:
                pass
        logger.debug("Result 'proc-%s': %s", self.name, results)
        return {'proc_%s' % self.name: results}


class NetworkPortStatsPlugin(NetworkPortStats):
    def __init__(self, port):
        self._external_net = IPSet(['10.1.0.0/16'])
        self._internal_net = IPSet(['172.16.0.0/16'])
        self._localhost = IPSet(['127.0.0.1/32'])
        super().__init__(port=int(port), networks=[self._external_net, self._internal_net, self._localhost])

    def run(self):
        super().run()
        logger.debug('Compute stats (port=%s): %s', self._port, self._stats)
        results = {
            'port_stats': {
                self._port:
                    {
                        'all': self._stats.to_dict(),
                        'external': self._subnet_stats[0].to_dict(),
                        'internal': self._subnet_stats[1].to_dict(),
                        'localhost': self._subnet_stats[2].to_dict()
                    }
            }
        }
        self._stats.reset()
        for stats in self._subnet_stats:
            stats.reset()
        logger.debug('results: %s', results)
        return results


class NetworkPortStatsByProcNamePlugin(Plugin):
    def __init__(self, proc_name):
        self._proc_name = proc_name
        proc_pid = None
        for proc in pu.process_iter():
            try:
                if proc_name in proc.name():
                    if proc.status() == pu.STATUS_ZOMBIE:
                        logger.warn("PID %d detected with %s but is ZOMBIE!" % (proc.pid, proc.name()))
                        continue
                    if proc.is_running():
                        proc_pid = proc.pid
                        break
            except pu.ZombieProcess:
                pass
        if not proc_pid:
            raise BadPluginException("ProcByNameMonitorPlugin:Process not found: {}".format(proc_name))

        proc = pu.Process(proc_pid)
        self._port_stats = [NetworkPortStatsPlugin(port) for port in
                            set([conn.laddr[1] for conn in proc.connections() if conn.status == 'LISTEN'])]

    def run(self):
        results = {}
        for port_stats in self._port_stats:
            results.update(port_stats.run())

        return {self._proc_name: results}
