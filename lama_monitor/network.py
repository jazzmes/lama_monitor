import sys
import logging
from scapy.all import sniff
from scapy.layers.inet import TCP, IP
from threading import Thread
from lama_monitor.base_plugin import Plugin
from time import sleep

from lama_monitor.utils.network import get_local_ip_addresses

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


logger = logging.getLogger(__name__)


class NetworkMonitor(object):

    def __init__(self):
        self._port_list = set()
        self._handlers = {}
        self._sniff_thread = None

    def add_port(self, port, handler):
        self._port_list.add(port)
        self._handlers[port] = handler

    def start(self):
        if not self._sniff_thread:
            ports_filter = 'tcp and ( port ' + ' or port '.join(str(p) for p in self._port_list) + ')'
            # start sniffing
            # sniff(filter="tcp and ( {port_list} )".format(port_list=ports_filter), prn=self.process_packet)
            logger.debug("Start sniff: %s", ports_filter)
            self._sniff_thread = Thread(target=sniff, kwargs={
                'filter': ports_filter,
                'prn': self.process_packet,
                'store': 0
            }, daemon=True)
            self._sniff_thread.start()

    def process_packet(self, packet):
        ip = packet[IP]
        hsrc = self._handlers.get(ip.sport)
        hdst = self._handlers.get(ip.dport)
        if hsrc:
            hsrc(packet)
        if hdst:
            hdst(packet)


class NetworkStats(object):

    def __init__(self):
        self.tx_packets = 0
        self.rx_packets = 0
        self.tx_bytes = 0
        self.rx_bytes = 0

    def __str__(self):
        return "tx = {} / {}, rx = {} / {}".format(self.tx_packets, self.tx_bytes, self.rx_packets, self.rx_bytes)

    def reset(self):
        self.tx_packets = 0
        self.tx_bytes = 0
        self.rx_packets = 0
        self.rx_bytes = 0

    def to_dict(self):
        return {
            'tx': {
                'packets': self.tx_packets,
                'bytes': self.tx_bytes
            },
            'rx': {
                'packets': self.rx_packets,
                'bytes': self.rx_bytes
            }
        }


class NetworkPortStats(Plugin):

    network_monitor = NetworkMonitor()

    def __init__(self, port, networks=None):
        # self._iface = 'p5p1'
        self._port = int(port)
        # TODO: get process name by port name
        self.network_monitor.add_port(self._port, self.process_packet)
        self._stats = NetworkStats()
        self._subnet_stats = []
        self._networks = networks or []
        self._local_address = get_local_ip_addresses()
        if networks:
            for net in networks:
                self._subnet_stats.append(NetworkStats())

    def process_packet(self, packet):
        ip = packet[IP]
        # if ip.sport == self._port:
        if ip.src in self._local_address:
            self._stats.tx_packets += 1
            self._stats.tx_bytes += len(packet)

            for idx, net in enumerate(self._networks):
                if ip.dst in net:
                    net_stats = self._subnet_stats[idx]
                    net_stats.tx_packets += 1
                    net_stats.tx_bytes += len(packet)
        else:
        # elif ip.dport == self._port:
            self._stats.rx_packets += 1
            self._stats.rx_bytes += len(packet)

            for idx, net in enumerate(self._networks):
                if ip.src in net:
                    net_stats = self._subnet_stats[idx]
                    net_stats.rx_packets += 1
                    net_stats.rx_bytes += len(packet)

        logger.debug("Packet: %s:%s -> %s:%s #=%s", ip.src, ip.sport, ip.dst, ip.dport, len(packet))

    def run(self):
        self.start()

    def start(self):
        self.network_monitor.start()


if __name__ == '__main__':
    ns1 = NetworkPortStats(port=80)
    ns2 = NetworkPortStats(port=6379)
    ns1.run()
    ns2.run()
    sys.stdout.flush()
    sleep(40)
    ns1.run()
    ns2.run()
