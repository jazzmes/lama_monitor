from collections import OrderedDict
from datetime import datetime as dt
import csv
import sys
import json

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


class OutputPlugin(object):

    def process_results(self, results):
        raise NotImplementedError()

    def stop(self):
        raise NotImplementedError()

    @staticmethod
    def _flatten_dict(d):
        new_d = {}
        for k, v in d.items():
            if isinstance(v, dict):
                flat_v = CSVOutput._flatten_dict(v)
                for k1, v1 in flat_v.items():
                    new_d["%s:%s" % (k, k1)] = v1
            else:
                new_d[k] = v
        return new_d


class CSVOutput(OutputPlugin):

    def __init__(self, filename, delimiter=','):
        self.headers = None
        self.filename = filename
        self.__csv_file = open(self.filename, "w")
        self.__csv_writer = csv.writer(self.__csv_file, delimiter=delimiter, quoting=csv.QUOTE_MINIMAL)

    def _initialize_headers(self, keys):
        self.headers = OrderedDict()
        for k in keys:
            if k not in self.headers:
                self.headers[k] = True

        self.append(['Timestamp'] + list(self.headers.keys()))

    def process_results(self, results):
        if not results:
            return
        results = self._flatten_dict(results)
        if not self.headers:
            self._initialize_headers(sorted(results.keys()))

        fields = [dt.now()]
        for k in self.headers:
            fields.append(results[k] if k in results else None)
        self.append(fields)

    def stop(self):
        self.__csv_file.close()

    def append(self, data):
        """
        Safer to store immediately... is performance hit negligible given granularity?
        :param data:
        :return:
        """
        self.__csv_writer.writerow(data)
        self.__csv_file.flush()


class JSONEventsOutput(OutputPlugin):

    def __init__(self, filename):
        self.headers = None
        self.filename = filename
        self.__json_events_file = open(self.filename, "w")

    def process_results(self, results):
        if not results:
            return

        results = self._flatten_dict(results)

        self.append({
            'timestamp': dt.strftime(dt.now(), "%Y-%m-%d %H:%M:%S.%f"),
            'data': results
        })

    def stop(self):
        self.__json_events_file.close()

    def append(self, data):
        """
        Safer to store immediately... is performance hit negligible given granularity?
        :param data:
        :return:
        """
        self.__json_events_file.write(json.dumps(data) + '\n')
        self.__json_events_file.flush()


class RedisOutput(OutputPlugin):

    def __init__(self, host='localhost', port='6379'):
        from lama_monitor.utils import KeyStore
        self._db = KeyStore(host=str(host), port=port)

    def process_results(self, results):
        for k, v in results.items():
            self._db.put_dict(k, v)

    def stop(self):
        pass


class StreamOutput(OutputPlugin):

    def __init__(self, stream=sys.stdout):
        self._stream = stream

    def process_results(self, results):
        self._stream.write("%s\n" % dt.now())
        for k, v in results.items():
            self._stream.write("\t%s: %s" % (k, v))
        self._stream.write("\n")
        self._stream.flush()

    def stop(self):
        pass