import re

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


NET_DEV_SPLIT = re.compile('[ |]+')


def get_process_disk_metrics(pid):
    lines = []
    results = {}
    proc_file = '/proc/%s/io' % pid
    try:
        with open(proc_file, 'r') as fp:
            lines = fp.readlines()
    except FileNotFoundError:
        pass

    for line in lines:
        try:
            k, v = (w.strip() for w in line.split(':'))
            results[k] = int(v)
        except ValueError as e:
            pass

    return results


def get_process_net_metrics(pid):
    lines = []
    results = {}
    proc_file = '/proc/%s/net/dev' % pid
    try:
        with open(proc_file, 'r') as fp:
            lines = fp.readlines()
    except FileNotFoundError:
        pass

    if lines:
        fields = NET_DEV_SPLIT.split(lines[1].strip())
        for line in lines[2:]:
            values = line.split()
            iface = values[0].strip().strip(':')
            # results.update({"%s:%s" % (iface, k): int(v) for k, v in zip(fields[1:], values[1:])})
            # info about the file: except for the first field (interface) all other fields are repeated to rx and tx
            results = {}
            field_set = set()
            for k, v in zip(fields[1:], values[1:]):
                field_name = "{}:{}:{}".format(iface, 'tx' if k in field_set else 'rx', k)
                field_set.add(k)
                results[field_name] = int(v)

    return results
