__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


class Plugin(object):

    def initialize(self):
        pass

    def run(self):
        raise NotImplementedError()

    def stop(self):
        pass
